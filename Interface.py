#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = "Lauréline Pierre"

import os
import tkinter
import tkinter as tk
from tkinter import *
from tkinter import filedialog as fd
from tkinter.messagebox import showinfo
import tkinter.font as font
from BWT import add_dollar, bwt_constructor, bwt_reconstructor
from Huffman_compression import Huffman_tree
from Interface_functions import get_popup_entry,get_press_button,popup_entry,get_binary_sequence


def creation_main_window():
    """
    Creation of the main window
    :return: main window
    """
    window = tk.Tk()  # creation of the main window
    window.title("Algorithmic project")  # setting title of main window
    window.geometry("1775x900")  # setting geometry of main window
    window.configure(bg="white")  # setting the color of the main window



    #creation of the frames
    bwt_frame = Frame(window, width=1775, height=450, bg="white", borderwidth=2,
                      relief="solid")  # creation of bwt frame
    huffman_frame = Frame(window, width=1775, height=400, bg="white", borderwidth=2,
                          relief="solid")  # creation of huffman frame
    bwt_frame_title = Label(bwt_frame, text="Burrows Weeler transform", bd=0, anchor="center",
                            font=("Arial", 25))  # creation of title of bwt frame
    huffman_frame_title = Label(huffman_frame, text="Huffman compression", bd=0, anchor="center",
                                font=("Arial", 25))  # creation of title of huffman frame
    bwt_frame.place(x=35, y=15)  # place the frame in the window
    huffman_frame.place(x=35, y=550)  # place the frame in the window
    bwt_frame_title.place(x=680, y=0)  # place the frame in the window
    huffman_frame_title.place(x=650, y=0)  # place the frame in the window

    bwt_choices_frame = LabelFrame(bwt_frame, text="Parameters", width=310, height=350, bg="white")  # frame for user
    # settings
    bwt_choices_frame.place(x=10, y=80)  # place bwt choices frame

    user_sequence_Label = Label(bwt_frame, text="", width=130, font=("Arial", 15), bg="white")  # creation of user
    # sequence label
    user_sequence_Label.place(x=330, y=100)  # place the label on main window

    bwt_showing_Label = Label(bwt_frame, text="", width=130, height=20, bg="white")  # label to display bwt sequene
    bwt_showing_Label.place(x=330, y=75)  # place the label

    #creation of the buttons

    validate_bwt_choices = tk.Button(bwt_choices_frame, text='Validate',
                                     command=lambda: get_press_button(user_sequence_Label, bwt_showing_Label, v))  #
    # button to validate user choices to show bwt sequence
    and_input_button = tk.Button(bwt_choices_frame, text='Enter a sequence',
                                 command=lambda: popup_entry(user_sequence_Label,
                                                             validate_bwt_choices, window, original_sequence_label))  # button to enter a sequence
    and_input_button.place(x=75, y=75)  # set the place of button

    v = IntVar()  # set as integer
    bwt_steps_display_button = Radiobutton(bwt_choices_frame, variable=v, value=1,
                                           text="Display directly the bwt sequence ")  # button radio to choose
    # displaying all steps of bwt
    bwt_direct_display_button = Radiobutton(bwt_choices_frame, variable=v, value=2,
                                            text="Display the bwt sequence step by step")  # button radio to choose
    # displaying directly bwt
    bwt_steps_display_button.place(x=25, y=145)  # place the button in the window
    bwt_direct_display_button.place(x=15, y=215)  # place the button in the window

    validate_bwt_choices.place(x=95, y=285)  # place the button in the window

    #creation of Huffman frame

    original_sequence_label = LabelFrame(huffman_frame, text="Original sequence", width=1500, height=60,
                                         bg="white")  # frame for the original sequence
    original_sequence_label.place(x=110, y=80)  # place the frame
    binary_sequence_label = LabelFrame(huffman_frame, text="Binary sequence", width=1500, height=60,
                                       bg="white")  # frame for the binary sequence
    binary_sequence_label.place(x=110, y=150)  # place the frame
    encoded_sequence_label = LabelFrame(huffman_frame, text="Encoded sequence", width=1500, height=60,
                                        bg="white")  # frame for the compressed sequence
    encoded_sequence_label.place(x=110, y=220)  # place the frame

    #compress button
    compress_sequence_button = tk.Button(huffman_frame, text='Compress', width=6, bg='#0052cc', fg='#ffffff',
                                         command=lambda: get_binary_sequence(
                                             binary_sequence_label, user_sequence_Label))  # link the button to
    compress_sequence_button.place(x=1650, y=250)  # place the button
    compress_sequence_button_font = font.Font(size=15)  # set font of the button
    compress_sequence_button['font'] = compress_sequence_button_font  # set font of the button

    #save button
    save_work = tk.Button(huffman_frame, text='Save', width=6, bg='#0052cc', fg='#ffffff')  # creation of the button
    save_work.place(x=1650, y=350)  # place the button
    save_work_font = font.Font(size=15)  # set the button font
    save_work['font'] = save_work_font  # set the button font

    window.mainloop()