#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = "Lauréline Pierre"

import os
import tkinter
import tkinter as tk
from tkinter import *
from tkinter import filedialog as fd
from tkinter.messagebox import showinfo
import tkinter.font as font
from BWT import add_dollar, bwt_constructor, bwt_reconstructor
from Huffman_compression import Huffman_tree


def get_popup_entry(sequence_entry, user_sequence_Label, validate_button, original_sequence_label):
    """
    Function to recover user dna entry
    :param sequence_entry:
    :param user_sequence_Label:
    :param validate_button:
    :param original_sequence_label:
    :return:the sequence given by the user
    """
    user_input = sequence_entry.get()  # variable where user entry is storage
    original_sequence_label["text"] = user_input  # input sequence will be put in Huffman frame
    user_sequence_Label["text"] = user_input  # input sequence will be put in bwt frame
    validate_button["command"] = lambda: (
        get_press_button(user_input))  # when the button is presses, the sequence appears


def get_press_button(user_sequence_Label, bwt_showing_Label, v):
    """
    Function to know which button is pressed and associate the corresponding function
    :param user_sequence_Label:
    :param bwt_showing_Label:
    :return: A certain result according the user choices
    """
    selected_button = v.get()  # recover the button pressed
    sequence_4_bwt = user_sequence_Label["text"]  # recover the text from the user sequence label
    resultat, sequences_list = bwt_constructor(
        sequence_4_bwt)  # use the user entry in parameter of bwt_constructor function
    if selected_button == 1:
        bwt_showing_Label["text"] = "The BWT sequence is : " + str(
            resultat)  # is button selected is 1, display directly bwt sequence

    else:  # else display the different steps of the construction
        bwt_showing_Label["text"] = "The BWT sequence is : " + str(resultat) + "\n" + \
                                    "The different steps are : " + "\n" + str(sequences_list)


def popup_entry(user_sequence_Label, validate_button,window, original_sequence_label):
    """
    Function to display a pop up in which the user will inform his/her sequence
    :param user_sequence_Label:
    :param validate_button:
    :return:
    """

    popup_window = Toplevel(window)  # creation of the popup
    popup_window.geometry("200x150")  # set the popup's geometry

    sequence_entry = Entry(popup_window, width=100)  # creation of the entry for user sequence
    sequence_entry.pack()  # display the pop up

    validate_sequence = tk.Button(popup_window, text='Validate',
                                  command=lambda: get_popup_entry(sequence_entry, user_sequence_Label,
                                                                  validate_sequence, original_sequence_label))
    validate_sequence.place(x=60, y=50)  # link validate button with different functions


def get_binary_sequence(binary_sequence_label,user_sequence_Label):
    """
    Function to get the dna sequence in binary sequence
    :param binary_sequence_label:
    :return: binary sequence
    """
    sequence_to_binary = user_sequence_Label["text"]  # put the user dna sequence from label in a variable
    sequence, three = Huffman_tree(sequence_to_binary)  # call the function H
    binary_sequence_label["text"] = sequence  # put the result in the label



