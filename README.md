###Algorithmic project 

Welcome to the Burrows Weeler Transform et Huffman compression 

Here you have access to a interface that allows you to enter a dna sequence and apply a Burrows Weeler transformation or Huffman compression. 
With the Burrows Weeler transform you can choose to have directly the bwt sequence or see the different steps. 
With the Huffman compression you can compress your DNA sequence. The interface will give you the compress sequence. 

###Table of contents 
* How to use the program 
* Software used 
* Other 
* Author 
* Link for the project 

###How to use the programe 

python3 .\main.py

###Sofware used 

python 3.8.10

### Other 
Because of the time, there are some lack in this projet : 
* bwt reconstructor in the interface 
* In Huffman, the transformation of binary sequence to caracters sequence
* the seleciton and the sauvegard of a file 

Thank you for your inderstanding 

###Author 
Lauréline Pierre 

###Link for the project 
https://gitlab.com/laureline_pierre/algorithmic-project.git
