#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = "Lauréline Pierre"


def add_dollar(seq):
    """Function that allows to add a dollar at the end of the sequence
    :param seq:
    :return: seq$
    """
    return seq + "$"  # return the dna sequence with a $ at the end


def bwt_constructor(seq, step=False):
    """The function generates all the shifts possible of $ in the sequence and puts them in a list
    :param seq:
    :return: obwt sequence and a list with all the $ shifts
    """
    new_seq = add_dollar(seq)  # call the add_dollar function
    idx = len(new_seq)  # get the index with the len
    sequences_list = []  # create an empty list
    while idx > 0:
        j = new_seq[idx:] + new_seq[:idx]  # shift of $ according the index
        sequences_list.append(j)  # append the shift sequence in the list
        idx -= 1  # in order to do all indexes

    sequences_list_sorted = sorted(sequences_list)  # sort the list
    resultat = ''  # create an empty string
    for seq in sequences_list_sorted:
        resultat += seq[-1]  # the last letter of each element of the list constitute the bwt sequence
    sequences_list = '\n'.join(sequences_list)  # put the list in column
    return resultat, sequences_list  # return the bwt sequence and the list


# resultat, sequences_list = bwt_constructor("ACTTGATC")
# print(sequences_list, resultat)

def bwt_reconstructor(bwt):
    """
    With the bwt sequence found the original dna sequence
    :param bwt:
    :return: original dna sequence
    """
    bwt_list = []  # empty list
    for i in bwt:
        bwt_list.append(i)  # put element of bwt in the list
    bwt_list.sort()  # sort the list

    for i in range(len(bwt) - 1):
        idx = 0
        for j in bwt:
            bwt_list[idx] = j + bwt_list[idx]  # add the letters of bwt in each index of the list
            idx += 1
        bwt_list.sort()  # sort the list

    for seq in bwt_list:
        if seq.endswith('$'):  # look for the sequence with $ at the end
            reconstructed_seq = seq[:-1]  # take the original seq without the $ at the end
    return reconstructed_seq  # return the original sequence

