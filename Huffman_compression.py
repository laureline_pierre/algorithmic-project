#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = "Lauréline Pierre"


class Nodes:
    """
    Creation of the Nodes class
    """

    def __init__(self, frequency, nucleotide, left=None, right=None):
        self.frequency = frequency  # probability of the symbol
        self.nucleotide = nucleotide  # the symbol
        self.left = left  # the left node
        self.right = right  # the right node
        self.address = ''  # the tree direction (0 or 1)


def nucleotides_frequencies(seq):
    """
    This function allows to count the number of nucleotides A, T, C and G in a dna sequence
    :param nucleotide_dictionnary : dictionnary with the nucleotides A,T,C et G and their frequence in the DNA sequence
    :return: returns the dictionnary
    """
    nucleotide_dictionnary = {}  # creation of a dictionnary
    for nuc in seq:  # for each nucletoide in the sequence
        if nuc in nucleotide_dictionnary:  # if the nucleotide is in the dictionnary
            nucleotide_dictionnary[nuc] += 1  # add +1 to the value
        else:
            nucleotide_dictionnary[nuc] = 1  # else creation of the clef for the nucleotide

    return nucleotide_dictionnary  # return the dictionnary


nucleotides_address = dict()


def nucleotides_address_tree(node, binary_code=''):
    """
    Function to look for the binary code of a nucleotide in Huffman tree and put it in a string
    :param node:
    :param binary_code:
    :return: dictionary with binary code associated with all nucleotides
    """
    binary_code += str(node.address)  # add in the string the nucleotide binary code

    if (node.left):  # if there is a left node
        nucleotides_address_tree(node.left,
                                 binary_code)  # call the function to associate a node with its binary code
    if (node.right):  # if there is a right node
        nucleotides_address_tree(node.right, binary_code)  # call the function to associate a node with its binary code

    else:
        nucleotides_address[node.nucleotide] = binary_code  # if there is no nodes, so we are at the leaf we put in the
        # dico the nucleotide and its binary code

    return nucleotides_address  # return the dico


""" A supporting function in order to get the encoded result """


def nucleotide_to_binary_sequence(dna, coding):
    """
    This function allow to convert the dna sequence in binary sequence
    :param dna:
    :param coding:
    :return: binary sequence
    """
    the_string = ''
    for element in dna:  # for each element in the seq
        the_string += str(coding[element])  # concatenate
    return the_string  # return the binary sequence


def Huffman_tree(dna):
    """
    This function allows to construct the huffman tree
    :param dna:
    :return: encoded dna anf first element of the_nodes
    """
    nucleotide_frequencies_data = nucleotides_frequencies(
        dna)  # for the sequence, use the function nucleotides_frequencies and put the results in a variable
    nucleotide_key = nucleotide_frequencies_data.keys()  # put the nucleotide's key in a variable
    the_nodes = []  # creation of a dictionary

    # converting symbols and probabilities into huffman tree nodes
    for nuc in nucleotide_key:  # we take each nucleotide
        the_nodes.append(Nodes(nucleotide_frequencies_data.get(nuc),
                               nuc))  # we add to the dictionary the nodes with the nucleotide and its frequency
        # creation des nodes

    while len(the_nodes) > 1:  # while there are elements in the dictionary
        the_nodes = sorted(the_nodes,
                           key=lambda x: x.frequency)  # we sort the dictionary according  nucleotide's frequency
        right = the_nodes[0]  # index 0
        left = the_nodes[1]  # index 1

        left.address = 1  # the left node will have 1 for its address
        right.address = 0  # the right node will have 0 for its address
        new_node = Nodes(left.frequency + right.frequency, left.nucleotide + right.nucleotide, left,
                         right)  # we take the smallest nodes to create new one

        the_nodes.remove(left)  # we remove the left node use to create a new one
        the_nodes.remove(right)  # we remove the left node use to create a new one
        the_nodes.append(new_node)  # we append the new node

    Huffman_tree = nucleotides_address_tree(the_nodes[0])  # call the function with first element of dico
    encoded_result = nucleotide_to_binary_sequence(dna, coding=Huffman_tree)
    return encoded_result, the_nodes[0]

